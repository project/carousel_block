INTRODUCTION
------------
# Carousel Block

* The module provide carousel based on content entity type with CRUD operations. 
* The content entity type contains a lot of field:
- name
- caption text
- image
- publish

* This module also provide a block with custom configuration rendering carousel 
on the site front.

* Built With [Slick](https://kenwheeler.github.io/slick/) - Slick js library

REQUIREMENTS
------------
* Carousel is based on slick library with a lot of configurable options.
* You don't need any extra libraries.

INSTALLATION
------------
* Install as usual, see http://drupal.org/node/895232 for further information.
* Using Composer :
```
composer require durpal/carousel_block
```

CONFIGURATION
------------
* Configure user permissions in Administration » People » Permissions:

MAINTAINERS
------------
Current maintainers:
* Adam Pietras : [PixelWeb](https://pixel-web.pl)
